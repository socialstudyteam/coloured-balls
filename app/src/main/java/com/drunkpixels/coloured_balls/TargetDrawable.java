package com.drunkpixels.coloured_balls;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.graphics.drawable.shapes.Shape;

import com.drunkpixels.coloured_balls.shapes.Triangle;


public class TargetDrawable extends ShapeDrawable {

    Shape currentShape = new Triangle();


    public TargetDrawable() {
        super(new RectShape());
//        currentShape.resize(60, 60);
        this.setShape(currentShape);
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
//        System.out.println("bounds " + getBounds());
    }
}
