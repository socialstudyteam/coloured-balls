package com.drunkpixels.coloured_balls;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.graphics.drawable.shapes.Shape;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * This code is from this tutorial:
 * http://gamecodeschool.com/android/building-a-simple-game-engine/
 */

public class SimpleGameEngine extends AppCompatActivity {

    // gameView will be the view of the game
    // It will also hold the logic of the game
    // and respond to screen touches as well
    GameView gameView;

    TargetDrawable targetShape;
    ShapeDrawable ball;
    float shapeScaleFactor = 0.4f;
    float currentRotationDegrees = 0f;
    int targetSize = 500;
    int ballStartX = 0;
    int ballPosX = -1;
    int ballStartY = 0;
    int ballPosY = -1;
    int ballVelocity = 400; // pixels / sec
    int ballRadius = 40;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        targetShape = new TargetDrawable();

        // Initialize gameView and set it as the view
        gameView = new GameView(this);
        setContentView(gameView);

    }

    // GameView class will go here
    // Here is our implementation of GameView
    // It is an inner class.
    // Note how the final closing curly brace }
    // is inside SimpleGameEngine

    // Notice we implement runnable so we have
    // A thread and can override the run method.
    class GameView extends SurfaceView implements Runnable {

        // This is our thread
        Thread gameThread = null;

        // This is new. We need a SurfaceHolder
        // When we use Paint and Canvas in a thread
        // We will see it in action in the draw method soon.
        SurfaceHolder ourHolder;

        // A boolean which we will set and unset
        // when the game is running- or not.
        volatile boolean playing;

        // A Canvas and a Paint object
        Canvas canvas;
        Paint paint;

        // This variable tracks the game frame rate
        long fps;

        // This is used to help calculate the fps
        private long timeThisFrame;

        // Declare an object of type Bitmap
        Bitmap bitmapBob;

        // Bob starts off not moving
        boolean isMoving = false;

        // He can walk at 150 pixels per second
        float walkSpeedPerSecond = 150;

        // He starts 10 pixels from the left
        float bobXPosition = 10;

        float canvasWidth;
        float canvasHeight;
        int centerOfDrawY;
        int centerOfDrawX;

        int frameCount = 0;

        Drawable target;

        // When the we initialize (call new()) on gameView
        // This special constructor method runs
        public GameView(Context context) {
            // The next line of code asks the
            // SurfaceView class to set up our object.
            // How kind.
            super(context);

            // Initialize ourHolder and paint objects
            ourHolder = getHolder();
            paint = new Paint();

            // Load Bob from his .png file
            bitmapBob = BitmapFactory.decodeResource(this.getResources(), R.drawable.bob);
        }

        @Override
        public void run() {
            while (playing) {

                // Capture the current time in milliseconds in startFrameTime
                long startFrameTime = System.currentTimeMillis();

                ++frameCount;

                // Update the frame
                update();

                // Draw the frame
                draw();

                // Calculate the fps this frame
                // We can then use the result to
                // time animations and more.
                timeThisFrame = System.currentTimeMillis() - startFrameTime;
                if (timeThisFrame > 0) {
                    fps = 1000 / timeThisFrame;
                }

            }

        }

        // Everything that needs to be updated goes in here
        // In later projects we will have dozens (arrays) of objects.
        // We will also do other things like collision detection.
        public void update() {

            // If bob is moving (the player is touching the screen)
            // then move him to the right based on his target speed and the current fps.
            if(isMoving){
                //bobXPosition = bobXPosition + (walkSpeedPerSecond / fps);
                ballPosY -= ballVelocity / fps;
                if (ballPosY < -ballRadius) {
                    resetBall();
                }

                // Detect collision with target
                detectCollision();
            }

        }

        private void resetBall() {
            isMoving = false;
            ballPosX = ballStartX;
            ballPosY = ballStartY;
        }

        private void detectCollision() {
            Rect targetBounds = targetShape.getBounds();
            Rect ballBounds = new Rect(ballPosX - ballRadius, ballPosY - ballRadius, ballPosX + ballRadius, ballPosY + ballRadius);

            if (Rect.intersects(targetBounds, ballBounds)) {
                System.out.println("HIT!!");
                resetBall();
            }
        }

        // Draw the newly updated scene
        public void draw() {

            // Make sure our drawing surface is valid or we crash
            if (ourHolder.getSurface().isValid()) {
                // Lock the canvas ready to draw
                // Make the drawing surface our canvas object
                canvas = ourHolder.lockCanvas();

                ballStartX = Math.round(canvas.getWidth() / 2);
                ballStartY = Math.round(canvas.getHeight() * 0.9f);
                if (ballPosX < 0 && ballPosY < 0) {
                    ballPosX = ballStartX;
                    ballPosY = ballStartY;
                }

                if (frameCount % 15 == 0) {
                    System.out.println("ballPosX: " + ballPosX + ", ballPosY: " + ballPosY);
                }

                // Draw the background color
                canvas.drawColor(Color.argb(255,  200, 200, 200));

                // Choose the brush color for drawing
                paint.setColor(Color.argb(255,  0, 0, 0));

                // Make the text a bit bigger
                paint.setTextSize(45);

                // Display the current fps on the screen
                canvas.drawText("FPS:" + fps, 20, 40, paint);

                // Draw the ball
                paint.setColor(Color.BLACK);
                canvas.drawCircle(ballPosX, ballPosY, ballRadius, paint);

                rotateCanvas();
                drawTarget();
                // restore the canvas
                canvas.restore();

                // Draw everything to the screen
                // and unlock the drawing surface
                ourHolder.unlockCanvasAndPost(canvas);
            }

        }

        private void rotateCanvas() {
            // rotate the canvas, one revolution in 6s
            float degreesPerSecond = 360 / 10;
            float secondsPerFrame = 1f / fps;
            float degreesPerFrame = degreesPerSecond * secondsPerFrame;
//            System.out.println("dPS " + degreesPerSecond + ", sPF " + secondsPerFrame + ", dPF " + degreesPerFrame + ", cRD " + currentRotationDegrees);
            canvas.save();
            currentRotationDegrees += degreesPerFrame;
            canvas.rotate(currentRotationDegrees, centerOfDrawX, centerOfDrawY);
        }

        private void drawTarget() {
            canvasWidth = canvas.getWidth();
            canvasHeight = canvas.getHeight();
            centerOfDrawY = Math.round(canvasHeight / 3);
            centerOfDrawX = Math.round(canvasWidth / 2);
//            System.out.println("CH " + canvasHeight + " CW " + canvasWidth + " C x,y " + centerOfDrawX + ", " + centerOfDrawY);
            //  center and draw the shape
//            Drawable triangle = getResources().getDrawable(R.drawable.ic_triangle);
//            target = getResources().getDrawable(R.drawable.ic_triangle);
            int left = centerOfDrawX - targetSize / 2;
            int top = centerOfDrawY - targetSize / 2;
            int right = centerOfDrawX + targetSize / 2;
            int bottom = centerOfDrawY + targetSize / 2;
//            triangle.setBounds(left, top, right, bottom);
//            triangle.draw(canvas);
//            target.setBounds(left, top, right, bottom);
//            target.draw(canvas);
            targetShape.getShape().resize((float)targetSize, (float)targetSize);
            targetShape.setBounds(left, top, right, bottom);
//            System.out.println("left " + left + ", top " + top + ", bottom " + bottom + ", right " + right);
//            System.out.println(targetShape.getBounds());
//            System.out.println(targetShape.getShape().getWidth() + " " + targetShape.getShape().getHeight());
            targetShape.draw(canvas);
        }

        // If SimpleGameEngine Activity is paused/stopped
        // shutdown our thread.
        public void pause() {
            playing = false;
            try {
                gameThread.join();
            } catch (InterruptedException e) {
                Log.e("Error:", "joining thread");
            }

        }

        // If SimpleGameEngine Activity is started theb
        // start our thread.
        public void resume() {
            playing = true;
            gameThread = new Thread(this);
            gameThread.start();
        }

        // The SurfaceView class implements onTouchListener
        // So we can override this method and detect screen touches.
        @Override
        public boolean onTouchEvent(MotionEvent motionEvent) {

            switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {

                // Player has touched the screen
                case MotionEvent.ACTION_DOWN:

                    // Set isMoving so Bob is moved in the update method
                    isMoving = true;

                    break;

                // Player has removed finger from screen
                //case MotionEvent.ACTION_UP:

                    // Set isMoving so Bob does not move
                    //isMoving = false;

                    //break;
            }
            return true;
        }
    }
    // More SimpleGameEngine methods will go here
    // This method executes when the player starts the game
    @Override
    protected void onResume() {
        super.onResume();

        // Tell the gameView resume method to execute
        gameView.resume();
    }

    // This method executes when the player quits the game
    @Override
    protected void onPause() {
        super.onPause();

        // Tell the gameView pause method to execute
        gameView.pause();
    }
}
