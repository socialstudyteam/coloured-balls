package com.drunkpixels.coloured_balls.shapes;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.shapes.Shape;



public class Triangle extends Shape {

    @Override
    public void draw(Canvas canvas, Paint paint) {

        float topLeftX = 0;
        float topLeftY = 0;

        paint.setStrokeWidth(15);
        paint.setColor(Color.BLACK);
        canvas.drawLine(Math.round(topLeftX),
                        Math.round(topLeftY),
                        Math.round(topLeftX),
                        Math.round(topLeftY + getHeight()),
                        paint);
        paint.setColor(Color.RED);
        canvas.drawLine(Math.round(topLeftX),
                        Math.round(topLeftY + getHeight()),
                        Math.round(topLeftX + getWidth()),
                        Math.round(topLeftY + getHeight() / 2),
                        paint);
        paint.setColor(Color.GREEN);
        canvas.drawLine(Math.round(topLeftX + getWidth()),
                        Math.round(topLeftY + getHeight() / 2),
                        Math.round(topLeftX),
                        Math.round(topLeftY),
                        paint);
    }
}
